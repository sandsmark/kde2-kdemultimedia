set(IDL_INCLUDES "")

get_target_property(ARTSFLOW_INCLUDE_PATHS kde2::artsflow INTERFACE_INCLUDE_DIRECTORIES)
foreach(path ${ARTSFLOW_INCLUDE_PATHS})
    set(IDL_INCLUDES -I${path} ${IDL_INCLUDES})
endforeach()

get_target_property(KMEDIA2_INCLUDE_PATHS kde2::kmedia2 INTERFACE_INCLUDE_DIRECTORIES)
foreach(path ${KMEDIA2_INCLUDE_PATHS})
    set(IDL_INCLUDES -I${path} ${IDL_INCLUDES})
endforeach()

add_custom_command(
    OUTPUT mpg123arts.h mpg123arts.cc
    COMMAND mcopidl -t ${IDL_INCLUDES} -I${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/mpg123arts.idl
    )

set(kmpg123_SRCS
    mpg123/audio.c
    mpg123/buffer.c
    mpg123/common.c
    mpg123/decode_2to1.c
    mpg123/decode_4to1.c
    mpg123/decode_ntom.c
    mpg123/getbits.c
    mpg123/httpget.c
    mpg123/layer1.c
    mpg123/layer2.c
    mpg123/layer3.c
    mpg123/readers.c
    mpg123/tabinit.c
    mpg123/vbrhead.c
    mpg123/xfermem.c

    # TODO: these are the generic, unoptimized ones
    mpg123/decode.c
    mpg123/dct64.c
    )

kde2_module(libmpg123arts
    SOURCES
        mpg123arts.cc mpg123PlayObject_impl.cpp dxhead.c
        mpg123arts.cc

        ${kmpg123_SRCS}
    INCLUDE_DIRECTORIES
        ${CMAKE_CURRENT_SOURCE_DIR}/kde
        ${CMAKE_CURRENT_SOURCE_DIR}/common
    COMPILE_DEFINITIONS
        -D_REENTRANT
        -DNOXFERMEM
        -DNO_EQUALIZER
        -DNO_DECODE_AUDIO
        -DNO_DECODE_FILE
        -DNO_DECODE_WAV
    LIBS
        kde2::artsflow
        kde2::kdeui
        kde2::kmedia2
        #PkgConfig::libmpg123
    )

install(
    FILES
        ${CMAKE_CURRENT_BINARY_DIR}/mpg123arts.mcoptype
        ${CMAKE_CURRENT_BINARY_DIR}/mpg123arts.mcopclass
    DESTINATION
        "${CMAKE_INSTALL_LIBDIR}/mcop"
)

install(
    FILES
        mpg123PlayObject.mcopclass
    DESTINATION
        "${CMAKE_INSTALL_LIBDIR}/mcop/Arts"
)
