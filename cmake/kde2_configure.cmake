function(create_kde2_config_header)
    include(CheckIncludeFiles)
    include(CheckFunctionExists)
    include(CheckStructHasMember)
    include(CheckCSourceCompiles)
    include(CheckSymbolExists)

    check_include_files(string.h HAVE_STRING_H)
    check_include_files(unistd.h HAVE_UNISTD_H)
    check_include_files(sys/acl.h ACL_H)
    check_include_files(sys/soundcard.h HAVE_SYS_SOUNDCARD_H)

    check_function_exists("setenv" HAVE_SETENV)
    list(APPEND CMAKE_REQUIRED_LIBRARIES acl)
    check_function_exists("usleep" HAVE_USLEEP)

    check_symbol_exists("bzCompress" "bzlib.h" BZ2_NO_PREFIX)
    if(NOT BZ2_NO_PREFIX)
        set(NEED_BZ2_PREFIX ON)
    endif()


    check_struct_has_member("struct tm" tm_sec "time.h;sys/time.h" TIME_WITH_SYS_TIME LANGUAGE C)

    if (Threads_FOUND)
        check_include_files(pthread.h HAVE_LIBPTHREAD)
    endif()

    set(KDE_COMPILER_VERSION ${CMAKE_CXX_COMPILER_VERSION})
    set(KDE_COMPILING_OS ${CMAKE_HOST_SYSTEM_NAME})
    set(KDE_DISTRIBUTION_TEXT "Restoration Project")

    # TODO: check this properly

    if (CMAKE_SYSTEM_PROCESSOR MATCHES "(x86$)|(X86$)")
        # IA32 only
        set(HAVE_X86_SSE ON) # In 2020 for gods sake
    endif()

    configure_file(${PROJECT_SOURCE_DIR}/config.h.in ${PROJECT_BINARY_DIR}/config.h)
    include_directories(${PROJECT_BINARY_DIR})
    add_definitions(-DHAVE_CONFIG_H)
endfunction()

