set(IDL_INCLUDES "")

get_target_property(ARTSFLOW_INCLUDE_PATHS kde2::artsflow INTERFACE_INCLUDE_DIRECTORIES)
foreach(path ${ARTSFLOW_INCLUDE_PATHS})
    set(IDL_INCLUDES -I${path} ${IDL_INCLUDES})
endforeach()

get_target_property(KMEDIA2_INCLUDE_PATHS kde2::kmedia2 INTERFACE_INCLUDE_DIRECTORIES)
foreach(path ${KMEDIA2_INCLUDE_PATHS})
    set(IDL_INCLUDES -I${path} ${IDL_INCLUDES})
endforeach()

add_custom_command(
    OUTPUT oggarts.h oggarts.cc
    COMMAND mcopidl -t ${IDL_INCLUDES} -I${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/oggarts.idl
    )

kde2_module(liboggarts
    SOURCES
        oggarts.cc oggPlayObject_impl.cpp

        ${kogg_SRCS}
    LIBS
        kde2::artsflow
        kde2::kmedia2
        PkgConfig::vorbisfile
    )

install(
    FILES
        ${CMAKE_CURRENT_BINARY_DIR}/oggarts.mcoptype
        ${CMAKE_CURRENT_BINARY_DIR}/oggarts.mcopclass
    DESTINATION
        "${CMAKE_INSTALL_LIBDIR}/mcop"
)

install(
    FILES
        oggPlayObject.mcopclass
    DESTINATION
        "${CMAKE_INSTALL_LIBDIR}/mcop/Arts"
)
